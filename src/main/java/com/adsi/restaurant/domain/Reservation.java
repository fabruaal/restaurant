package com.adsi.restaurant.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Reservation {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column (columnDefinition = "serial")
    private int id;

    private String locator;
    private Long latitude;
    private Long longitude;
    private int person;
    private Date date;
    private String turn;

    @ManyToOne
    private Restaurant restaurant;

}
